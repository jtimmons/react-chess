/* -*- mode: js-jsx; -*- */

import React from 'react';
import ChessboardModel from '../model/board.js';
import GameController from '../controller/game.js';
import Cell from './cell.js';

import './board.css';

class Chessboard extends React.Component {
    constructor() {
	super();

	this.state = {
	    game: new GameController( new ChessboardModel() )
	}

	this.state.game.onUpdate = this.onUpdate();
    }

    render() {
	var rows = [];
	for (var i=0; i < this.state.game.board.board.length; i++) {
	    var row = [];
	    	    
	    for (var j=0; j < this.state.game.board.board.length; j++) {
		row.push( <Cell key={(i,j)}
			  model={this.state.game.board.board[i][j]}
			  controller={this.state.game} />
			);
	    }
	    
	    rows.push( <tr key={i}>{row}</tr> );
	}

	var winner = this.state.game.winner ? <h1>Winner: {this.state.game.winner}!</h1> : <div />;
	var stalemate = this.state.game.stalemate ? <h1>Stalemate! Everyone loses!</h1> : <div />;

	return (
	    <div>
	      {winner}
	      {stalemate}
	      <p>Player {this.state.game.turn}s Turn</p>
	      <table className="chessboard">
		<tbody>
		  {rows}
		</tbody>
	      </table>
	    </div>
	);
    }

    onUpdate() {
	var component = this;
	return function() {
	    component.forceUpdate();
	}
    }
}

export default Chessboard
