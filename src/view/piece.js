/* -*- mode: js-jsx; -*- */

import React from 'react';

import { Pieces, Allegiances } from '../model/piece.js';

import black_bishop from '../img/pieces/black/bishop.png';
import black_king   from '../img/pieces/black/king.png';
import black_knight from '../img/pieces/black/knight.png';
import black_pawn   from '../img/pieces/black/pawn.png';
import black_queen  from '../img/pieces/black/queen.png';
import black_rook   from '../img/pieces/black/rook.png';
import white_bishop from '../img/pieces/white/bishop.png';
import white_king   from '../img/pieces/white/king.png';
import white_knight from '../img/pieces/white/knight.png';
import white_pawn   from '../img/pieces/white/pawn.png';
import white_queen  from '../img/pieces/white/queen.png';
import white_rook   from '../img/pieces/white/rook.png';


var images = {
    [Allegiances.BLACK]: {
	[Pieces.PAWN]:   black_pawn,
	[Pieces.BISHOP]: black_bishop,
	[Pieces.KNIGHT]: black_knight,
	[Pieces.ROOK]:   black_rook,
	[Pieces.QUEEN]:  black_queen,
	[Pieces.KING]:   black_king
    },
    [Allegiances.WHITE]: {
	[Pieces.PAWN]:   white_pawn,
	[Pieces.BISHOP]: white_bishop,
	[Pieces.KNIGHT]: white_knight,
	[Pieces.ROOK]:   white_rook,
	[Pieces.QUEEN]:  white_queen,
	[Pieces.KING]:   white_king
    }
};

class Piece extends React.Component {
    render() {

	var image = images[this.props.model.allegiance][this.props.model.piece];

	return (
	    <img src={image} alt={this.props.model.piece} draggable="true"
		 onDragStart={this.props.controller.onDragStart()}
		 data-row={this.props.model.row}
		 data-col={this.props.model.col}/>
	);
    }
}

export default Piece;
