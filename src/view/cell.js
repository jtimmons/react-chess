/* -*- mode: js-jsx; -*- */

import React from 'react';
import Piece from './piece.js';


function is_even(num) {
    return num % 2 === 0
};


class Cell extends React.Component {

    render() {
	var color = this.isBlack() ? "black" : "white";
	var classes = ["cell", color].join(" ");
	var contents = null;

	if (this.props.model.piece){
	    contents = <Piece model={this.props.model.piece} controller={this.props.controller} />;
	}

	return (
	    <td className={classes}
		onDragOver={this.props.controller.allowDrop}
		onDrop={this.props.controller.onDrop()}
		data-row={this.props.model.row}
		data-col={this.props.model.col}>
	      {contents}
	    </td>
	);
    }

    isBlack() {
	return ((is_even(this.props.model.row) && !is_even(this.props.model.col)) ||
		(!is_even(this.props.model.row) && is_even(this.props.model.col)));
    }
}

export default Cell;
