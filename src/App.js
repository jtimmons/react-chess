/* -*- mode: js-jsx; -*- */

import React from 'react';
import Chessboard from './view/board.js';
import './App.css';

class App extends React.Component {
  render() {
    return (
       <div className="App">
	 <div id="Header">
	   <h1>Welcome to Chess!</h1>
	 </div>
	 <div id="Game">
	   <Chessboard />
	 </div>
       </div>
    );
  }
}

export default App;
