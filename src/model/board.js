import CellModel from './cell.js';
import {PieceModel, Pieces, Allegiances} from './piece.js';

class ChessboardModel {
    constructor() {
        this.board = [];

        for (var i=0; i<8; i++) {
            var row = [];
            for (var j=0; j<8; j++) {
                row.push( new CellModel(i, j) );
            }
            
            this.board.push(row);
        }

	this.init();
    }

    init() {

	var backrow = [Pieces.ROOK, Pieces.KNIGHT, Pieces.BISHOP,
		       Pieces.QUEEN, Pieces.KING,
		       Pieces.BISHOP, Pieces.KNIGHT, Pieces.ROOK];

	/*************************
	 * Clear the board
	 *************************/
	for (var i=0; i < this.board.length; i++) {
	    for (var j=0; j < this.board[0].length; j++) {
		this.board[0][i].piece = null;
	    }
	}

	/*************************
	 * Set black pieces
	 *************************/
	for (i=0; i < this.board[0].length; i++) {
	    this.board[0][i].piece = new PieceModel(backrow[i], Allegiances.BLACK, 0, i);
	}

	for (i=0; i < this.board[1].length; i++) {
	    this.board[1][i].piece = new PieceModel(Pieces.PAWN, Allegiances.BLACK, 1, i);
	}

	/*************************
	 * Set white pieces
	 *************************/
	for (i=0; i < this.board[6].length; i++) {
	    this.board[6][i].piece = new PieceModel(Pieces.PAWN, Allegiances.WHITE, 6, i);
	}

	for (i=0; i < this.board[7].length; i++) {
	    this.board[7][i].piece = new PieceModel(backrow[i], Allegiances.WHITE, 7, i);
	}
    }
}

export default ChessboardModel;
