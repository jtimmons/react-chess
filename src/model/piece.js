

var Pieces = {
    PAWN:   "pawn",
    BISHOP: "bishop",
    KNIGHT: "knight",
    ROOK:   "rook",
    QUEEN:  "queen",
    KING:   "king"
};


var Allegiances = {
    WHITE: "white",
    BLACK: "black"
};


class PieceModel {
    constructor(piece, allegiance, row, col) {
	this.piece = piece;
	this.allegiance = allegiance;
	this.row = row;
	this.col = col;
    }
}


module.exports = {
    PieceModel: PieceModel,
    Allegiances: Allegiances,
    Pieces: Pieces
}
