
import Chess from 'chess';

import { Pieces, Allegiances } from '../model/piece.js';


var Players = {
    PLAYER1: Allegiances.WHITE,
    PLAYER2: Allegiances.BLACK
}


class GameController {
    constructor(board) {
	this.board = board;
	this.turn  = Players.PLAYER1;
	this.winner = null;
	this.stalemate = null;
	this.onUpdate = null;
	this.engine = Chess.create();
    }

    allowDrop(event) {
	event.preventDefault();
    }

    onDragStart() {
	var controller = this;

	return function(event) {

	    // Don't allow players to do anything after game is over
	    if ( controller.winner || controller.stalemate ) {
		console.log("Game over - ignoring drag event");
		event.preventDefault();
	    }
	    
	    var row = event.target.attributes["data-row"].value;
	    var col = event.target.attributes["data-col"].value;
	    var piece = controller.board.board[row][col].piece;

	    // Only allow the drag to begin if we control the piece
	    if ( piece !== null && piece.allegiance === controller.turn ) {
		event.dataTransfer.setData("row", row);
		event.dataTransfer.setData("col", col);
	    }
	    else {
		console.log("Ignoring drag event on enemy piece");
		event.preventDefault();
	    }
	}
    }

    onDrop() {
	var controller = this;

	return function(event) {
	    var src_row = event.dataTransfer.getData("row");
	    var src_col = event.dataTransfer.getData("col");
	    var dst_row = event.target.attributes["data-row"].value;
	    var dst_col = event.target.attributes["data-col"].value;

	    if ( move_is_valid(controller.board, src_row, src_col, dst_row, dst_col) ) {
		console.log("Moving (%d, %d) => (%d, %d)", src_row, src_col, dst_row, dst_col);

		try {
		    var result = controller.move(src_row, src_col, dst_row, dst_col);

		    // Endgame conditions
		    controller.stalemate = result.isStalemate
		    if ( result.isCheckmate === true ) {
			controller.winner = controller.turn;
		    }

		    controller.turn = switch_player(controller.turn);
		}
		catch (e) {
		    console.log(e);
		    event.preventDefault();
		}
	    }
	    else {
		console.log("Move invalid - dropping");

		event.preventDefault();
	    }

	    // Fire an update event
	    if (controller.onUpdate) {
		controller.onUpdate();
	    }
	}
    }

    move(srcRow, srcCol, dstRow, dstCol) {
	var piece = get_piece(this.board, srcRow, srcCol);

	// Tell engine that we're moving from src to dst
	// Throws an exception if it's invalid
	this.engine.move( algebraic(piece, dstRow, dstCol) );
	
	this.board.board[dstRow][dstCol].piece = piece;
	this.board.board[dstRow][dstCol].piece.row = dstRow;
	this.board.board[dstRow][dstCol].piece.col = dstCol;

	this.board.board[srcRow][srcCol].piece = null;

	return this.engine.getStatus();
    }
}

function algebraic(piece, row, col) {
    var col_name = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];
    var piece_name = {
	[Pieces.PAWN]:   '',
	[Pieces.ROOK]:   'R',
	[Pieces.KNIGHT]: 'N',
	[Pieces.BISHOP]: 'B',
	[Pieces.QUEEN]:  'Q',
	[Pieces.KING]:   'K'
    };

    return piece_name[piece.piece] + col_name[col] + (7 - row + 1);
}


function switch_player(player) {
    return (player === Players.PLAYER1) ? Players.PLAYER2 : Players.PLAYER1;
}


function get_cell(board, row, col) {
    return board.board[row][col];
}


function get_piece(board, row, col) {
    return get_cell(board, row, col).piece;
}


function move_is_valid(board, srcRow, srcCol, dstRow, dstCol) {
    var src_piece = get_piece(board, srcRow, srcCol);
    var dst_piece = get_piece(board, dstRow, dstCol);
    var valid_positions = [];

    var row_positions = function(row, col) {
	var positions = [];
	
	for (var i=col; i < 8; i++) {
	    
	}

	return positions;
    }

    // Cannot move to square that's already occupied by one of our units
    if ( dst_piece && dst_piece.allegiance === src_piece.allegiance ) {
	return false;
    }

    if ( src_piece.piece === Pieces.PAWN ) {
	// TODO: Do something about first movement stuff and 'en passent' moves
	valid_positions = []
    }

    if ( src_piece.piece === Pieces.ROOK ) {
	valid_positions = [];
    }

    return true;
}


export default GameController;
